# Contributor: TBK <alpine@jjtc.eu>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Pablo Correa Gomez <pabloyoyoista@postmarketos.org>
# Maintainer: Pablo Correa Gomez <pabloyoyoista@postmarketos.org>
pkgname=libportal
pkgver=0.9.0
pkgrel=0
pkgdesc="GIO-style async APIs for most Flatpak portals"
url="https://github.com/flatpak/libportal"
arch="all"
license="LGPL-3.0-or-later"
makedepends="
	meson
	glib-dev
	gtk-doc
	gobject-introspection-dev
	vala
	gtk+3.0-dev
	gtk4.0-dev
	qt5-qtbase-dev
	qt5-qttools-dev
	qt5-qtx11extras-dev
	qt6-qtbase-dev
	qt6-qtbase-private-dev
	qt6-qttools-dev
	"
checkdepends="xvfb-run"
subpackages="
	$pkgname-dev
	$pkgname-gtk3:_gtk3
	$pkgname-gtk4:_gtk4
	$pkgname-qt5:_qt5
	$pkgname-qt6:_qt6
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/flatpak/libportal/archive/$pkgver.tar.gz"

build() {
	abuild-meson \
		-Dbackend-gtk3=enabled \
		-Dbackend-gtk4=enabled \
		-Dbackend-qt5=enabled \
		-Dbackend-qt6=enabled \
		-Ddocs=false \
		. output
	meson compile -C output
}

check() {
	xvfb-run -s '-nolisten local' -a meson test -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

_gtk3() {
	pkgdesc="$pkgdesc (GTK+3.0 backend)"
	# for gobject instrospection
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/libportal-gtk3*
	amove usr/lib/girepository-1.0/XdpGtk3*
}

_gtk4() {
	pkgdesc="$pkgdesc (GTK4.0 backend)"
	# for gobject instrospection
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/lib/libportal-gtk4*
	amove usr/lib/girepository-1.0/XdpGtk4*
}

_qt5() {
	pkgdesc="$pkgdesc (Qt5 backend)"
	depends=""

	amove usr/lib/libportal-qt5*
}

_qt6() {
	pkgdesc="$pkgdesc (Qt6 backend)"
	depends=""

	amove usr/lib/libportal-qt6*
}

sha512sums="
2a297654b14cfa7634ee3038a645158913a42c8d428a70a49aa6f8df700a73d4ad11d53ad0343e2bab5fe35f5bc521771941abffdc0f7b90b7c2206e1f85965f  libportal-0.9.0.tar.gz
"
